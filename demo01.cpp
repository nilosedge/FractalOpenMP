#include <libiomp/omp.h>
#include <stdio.h>

int main() {
	#pragma omp parallel
	{
		printf("Hello world! %i\n", omp_get_thread_num());
	}

	#pragma omp parallel num_threads(10)
	{
		printf("Ten Threads!!! %i\n", omp_get_thread_num());
	}
}
