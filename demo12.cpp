#include <iostream>

int main() {

	int i = 0;
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			#pragma omp critical (name)
			{
				#pragma omp parallel
				{
					#pragma omp single
					{
						i++;
					}
				}
			}
		}

		#pragma omp section
		{
			#pragma omp critical (name)
			{
				#pragma omp parallel
				{
					#pragma omp single
					{
						i++;
					}
				}
			}
		}
	}
	// What is going to be the value of i?
	std::cout << i << std::endl;
}
