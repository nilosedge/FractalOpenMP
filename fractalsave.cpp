#include <png.h>
#include <complex>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <sys/stat.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iomanip>
	
using namespace std;

typedef complex<long double> cplx;

typedef struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} pixel_t;


static int save_png_to_file (int width, int height, pixel_t *pixels, const char *path) {

	FILE * fp;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	size_t x, y;
	png_byte ** row_pointers = NULL;
	/* "status" contains the return value of this function. At first
		it is set to a value which means 'failure'. When the routine
		has finished its work, it is set to a value which means
		'success'. */
	int status = -1;
	/* The following number is set by trial and error only. I cannot
		see where it it is documented in the libpng manual.
	*/
	int pixel_size = 3;
	int depth = 8;
	
	fp = fopen (path, "wb");
	if (! fp) {
		goto fopen_failed;
	}

	png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		goto png_create_write_struct_failed;
	}
	
	info_ptr = png_create_info_struct (png_ptr);
	if (info_ptr == NULL) {
		goto png_create_info_struct_failed;
	}
	
	/* Set up error handling. */

	if (setjmp (png_jmpbuf (png_ptr))) {
		goto png_failure;
	}
	
	/* Set image attributes. */

	png_set_IHDR (png_ptr, info_ptr, width, height, depth, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	
	/* Initialize rows of PNG. */

	row_pointers = png_malloc (png_ptr, height * sizeof (png_byte *));
	for (y = 0; y < height; ++y) {
		png_byte *row = png_malloc (png_ptr, sizeof (uint8_t) * width * pixel_size);
		row_pointers[y] = row;
		for (x = 0; x < width; ++x) {
			pixel_t *pixel = pixels + width * y + x;
			*row++ = pixel->red;
			*row++ = pixel->green;
			*row++ = pixel->blue;
		}
	}
	
	/* Write the image data to "fp". */

	png_init_io (png_ptr, fp);
	png_set_rows (png_ptr, info_ptr, row_pointers);
	png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	/* The routine has successfully written the file, so we set
		"status" to a value which indicates success. */

	status = 0;
	
	for (y = 0; y < height; y++) {
		png_free (png_ptr, row_pointers[y]);
	}
	png_free (png_ptr, row_pointers);
	
 png_failure:
 png_create_info_struct_failed:
	png_destroy_write_struct (&png_ptr, &info_ptr);
 png_create_write_struct_failed:
	fclose (fp);
 fopen_failed:
	return status;
}

int main () {

	int width = 1280;
	int height = 800;
	int maxiter = 100;

	long double zoomPointX = 0.0016437219687502394;
	long double zoomPointY = -0.8224676332993273;

	long double halfw = ((double)width / (double)2);
	long double halfh = ((double)height / (double)2);

	long double sc = (height/2);
	long double tx = halfw - (zoomPointX * sc);
	long double ty = ((zoomPointY * sc) + halfh);

	long double x = 0;
	long double y = 0;

	long double cr = 0;
	long double ci = 0;

	long double zr = 0;
	long double zi = 0;

	long double zrs = 0;
	long double zis = 0;

	int colorMax = 256;

	pixel_t *colors = calloc (sizeof (pixel_t), colorMax * 7);

	for(int i = 0; i < colorMax; i++) {
		int cm = colorMax - 1;
		pixel_t *color = colors + (colorMax * 0) + i;
		color->red = i; color->green = 0; color->blue = 0;
		color = colors + (colorMax * 1) + i;
		color->red = cm; color->green = i; color->blue = 0;
		color = colors + (colorMax * 2) + i;
		color->red = cm - i; color->green = cm; color->blue = 0;
		color = colors + (colorMax * 3) + i;
		color->red = 0; color->green = cm; color->blue = i;
		color = colors + (colorMax * 4) + i;
		color->red = 0; color->green = cm - i; color->blue = cm;
		color = colors + (colorMax * 5) + i;
		color->red = i; color->green = 0; color->blue = cm;
		color = colors + (colorMax * 6) + i;
		color->red = cm - i; color->green = 0; color->blue = cm - i;
	}

	pixel_t *pixels = calloc (sizeof (pixel_t), width * height);

	#pragma omp parallel
	{
		printf("Hello!\n");
	}
	usleep(1000000);
	
	long double mu = 0;
	pixel_t *color = 0;

	#pragma omp parallel for ordered schedule(dynamic)
	for(int zl = 0; zl < 1000; zl++) {

		stringstream ss;
		ss << "files/fruit";
		ss << setfill('0') << setw(5) << zl;
		ss << ".png";

		struct stat buffer;
		if(stat (ss.str().c_str(), &buffer) != 0) {

			#pragma omp parallel for collapse(2)
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {

					pixel_t *pixel = pixels + width * j + i;

					cr = (i - tx) / sc;
					ci = (ty - j) / sc;

					int n = 0;
					maxiter = (zl * 3) + 50;

					zr = 0;
					zi = 0;

					zrs = 0;
					zis = 0;

					while(zrs + zis < 4.0 && n < maxiter) {
						zi = zr * zi;
						zi += zi;
						zi += ci;
						zr = zrs - zis + cr;
						zrs = zr * zr;
						zis = zi * zi;
						n++;
					}

					long double mu = (n + 1 - log(abs(log(zrs + zis)))/log(2)) / (long double)(maxiter + 1);
					if(mu < 0) mu = 0;
					if(mu > 1) mu = 1;

					color = colors + (int)((colorMax * 7) * mu);
					pixel->red = color->red;
					pixel->green = color->green;
					pixel->blue = color->blue;
				}
			}

			#pragma omp ordered
			save_png_to_file (width, height, pixels, ss.str().c_str());
			cout << "Finished Write: " << ss.str() << endl;
		} else {
			cout << "Skipping file: " << ss.str() << endl;
		}

		x = (tx - halfw) / sc;
		y = (ty - halfh) / sc;
		sc *= 1.05;
		tx = ((x * sc) + (long double)halfw);
		ty = ((y * sc) + (long double)halfh);

		cout << x << "," << y << endl;
		cout << zoomPointX << "," << zoomPointY << endl;
		cout << sc << "," << sc << endl;

	}

	return 0;
}
