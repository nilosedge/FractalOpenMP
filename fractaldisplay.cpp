#include <complex>
#include <iostream>
#include <cstdio>
#include <unistd.h>

using namespace std;
 
typedef complex<long double> cplx;
 
#define clear() printf("\033[H\033[J")
#define gotoxy(x, y) printf("\033[%d;%dH", x, y)

int main() {

	const int w = 170;
	const int h = 50;

	const double halfw = ((double)w / (double)2);
	const double halfh = ((double)h / (double)2);

	const double zoomPointX = 0.0016437219687502394;
	const double zoomPointY = -0.8224676332993273;
	const double maxIter = 1000;

	long double sx = (h/2);
	long double sy = (h/2);
	long double tx = halfw - (zoomPointX * sx);
	long double ty = ((zoomPointY * sy) + halfh);

	long double xc = 0;
	long double yc = 0;

	char *buffer = new char[w*h];
	static const char charset[] = ".,c8M@jawrpogOQEPGJ";
	char ch = ' ';

	for(int zl = 0; zl < 3000; zl++) {
		for(int j = 0; j < h; j++) {
			for(int i = 0; i < w; i++) {
				
				cplx c = cplx((i - tx) / sx, (ty - j) / sy);

				cplx z = c;
				int n=0;
				for(; n<maxIter; ++n) {
					if( std::abs(z) >= 4.0) break;
					z = z*z + c;
				}

				if(n >= maxIter) n = 0;
				if(n <= 0) n = 0;

				//long double mu = (n + 1 - log(abs(log(z)))/log(2)) / (long double)(maxIter + 1);
				//if(mu < 0) mu = 0;
				//if(mu > 1) mu = 1;
				//int color = ((int)(24 * mu)) - 1;

				ch = charset[n % (sizeof(charset)-1)];
				//cout << "\033[38;5;" << color << "m";
				//cout << "\033[38;5;" << 248 << "m";
				//cout << "\033[38;5;" << (n % (sizeof(charset)-1)) + 245 << "m"; putchar(ch); cout << "\033[0m";
				putchar(ch);
			}
			cout << endl;
		}
		gotoxy(0,0);

		long double x = (tx - halfw) / sx;
		sx *= 1.01;
		tx = ((x * sx) + halfw);

		long double y = (ty - halfh) / sy;
		sy *= 1.01;
		ty = ((y * sy) + halfh);

	}
}
