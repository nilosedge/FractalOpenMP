#include <iostream>

int main() {
	
	int k = 0;
	int array[10];
	for(int i = 0; i < 10; i++) array[i] = 0;

	#pragma omp parallel for
	for(int i = 0; i < 10000; i += k) {
		#pragma omp critical (lockname)
		{
			k = array[i % 10];
			i += k;
			array[i % 10] = k;
		}
	}

}
