#CC=clang-omp++
CC=g++-5
CFLAGS=-fopenmp -O3 -fpermissive
INCLUDES=-I/usr/local/include
LIBS=-lpng -L/usr/local/lib

PROGS=demo01 demo02 demo03 demo04 demo05 demo06 demo07 demo08 demo09 demo10 demo11 demo12 demo13 demo14 fractaldisplay fractaldisplay_omp fractalsave

all: $(PROGS)

.cpp:
	$(CC) $(CFLAGS) $(INCLUDES) $(LIBS) $< -o $@

clean:
	rm -fr $(PROGS)
