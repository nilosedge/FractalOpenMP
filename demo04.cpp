#include <iostream>

int main() {

	#pragma omp parallel for
	for(int n=0; n<10; ++n) {
		std::cout << n;
	}
	std::cout << std::endl;

	#pragma omp parallel for
	for(char n=65; n<75; ++n) {
		std::cout << n;
	}
	std::cout << std::endl;

	#pragma omp parallel
	{
		#pragma omp for
		for(int n=0; n<10; ++n) {
			std::cout << n;
		}
		std::cout << std::endl;

		#pragma omp for
		for(char n=65; n<75; ++n) {
			std::cout << n;
		}
		std::cout << std::endl;
	}


}
