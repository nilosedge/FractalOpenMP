#include <cmath>
#include <iostream>
#include <iomanip>

int main() {

	#pragma omp parallel
	{
		#pragma omp for nowait
		for(int n=0; n<10; ++n) {
			std::cout << n;
		}

		#pragma omp for nowait
		for(char n=65; n<75; ++n) {
			std::cout << n;
		}

		#pragma omp barrier
		#pragma omp single
		std::cout << std::endl;
	}

}
