#include <libiomp/omp.h>
#include <stdio.h>

int method1(int id) { printf("Method 1 %i\n", id); }
int method2(int id) { printf("Method 2 %i\n", id); }
int method3(int id) { printf("Method 3 %i\n", id); }
int method4(int id) { printf("Method 4 %i\n", id); }
int method5(int id) { printf("Method 5 %i\n", id); }
int method6(int id) { printf("Method 6 %i\n", id); }

int main() {

	#pragma omp parallel sections
	{
		#pragma omp section
		method1(omp_get_thread_num());
		#pragma omp section
		method2(omp_get_thread_num());
		#pragma omp section
		method3(omp_get_thread_num());
		#pragma omp section
		method4(omp_get_thread_num());
		#pragma omp section
		method5(omp_get_thread_num());
		#pragma omp section
		method6(omp_get_thread_num());
	}

}
