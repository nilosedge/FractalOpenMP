#include <complex>
#include <iostream>
#include <cstdio>
#include <unistd.h>

using namespace std;
 
typedef complex<long double> cplx;

#define clear() printf("\033[H\033[J")
#define gotoxy(x, y) printf("\033[%d;%dH", x, y)
 
int MandelbrotCalculate(cplx c, int maxiter) {
	cplx z = c;
	int n=0;
	for(; n<maxiter; ++n) {
		if( std::abs(z) >= 4.0) break;
		z = z*z + c;
	}
	return n;
}

int main() {

	const int w = 170;
	const int h = 50;

	const double halfw = ((double)w / (double)2);
	const double halfh = ((double)h / (double)2);

	const double zoomPointX = 0.0016437219687502394;
	const double zoomPointY = -0.8224676332993273;
	const double maxIter = 2000;

	long double sx = (h/2);
	long double sy = (h/2);
	long double tx = halfw - (zoomPointX * sx);
	long double ty = ((zoomPointY * sy) + halfh);

	long double xc = 0;
	long double yc = 0;

	char *buffer = new char[w*h];
	static const char charset[] = ".,c8M@jawrpogOQEPGJ";
	char ch = ' ';

	#pragma omp parallel
	{
		printf("Hello!\n");
	}
	usleep(1000000);
	
	#pragma omp parallel for ordered schedule(dynamic)
	for(int zl = 0; zl < 3000; zl++) {

		#pragma omp parallel for collapse(2)
		for(int i = 0; i < w; i++) {
			for(int j = 0; j < h; j++) {
			
				cplx c = cplx((i - tx) / sx, (ty - j) / sy);
							
				int n = MandelbrotCalculate(c, maxIter);

				if(n >= maxIter) n = 0;
				if(n <= 0) n = 0;

				ch = charset[n % (sizeof(charset)-1)];
				buffer[(j*w) + i] = ch;
				//putchar(buffer[(j*i) + i]);
			}
		}

		#pragma omp ordered
		for(int j = 0; j < h; j++) {
			for(int i = 0; i < w; i++) {
				putchar(buffer[(j*w) + i]);
			}
			cout << endl;
		}
		gotoxy(0, 0);

		long double x = (tx - halfw) / sx;
		sx *= 1.01;
		tx = ((x * sx) + halfw);

		long double y = (ty - halfh) / sy;
		sy *= 1.01;
		ty = ((y * sy) + halfh);

	}
}
