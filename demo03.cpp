#include <iostream>

int main() {

	#pragma omp parallel
	{
		printf("Hello!\n");
		#pragma omp for
		for(int n=0; n<10; ++n) {
			std::cout << n;
		}
	}
	std::cout << std::endl;


	#pragma omp parallel for
	for(int n=0; n<10; ++n) {
		std::cout << n;
	}
	std::cout << std::endl;


	#pragma omp parallel
	{
		#pragma omp for ordered
		for(int n=0; n<10; ++n) {
			#pragma omp ordered
			std::cout << n;
		}
	}
	std::cout << std::endl;

}
