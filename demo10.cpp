#include <libiomp/omp.h>
#include <cmath>
#include <stdio.h>

void calculate(int *x, int start, int amount) {
	printf("Starting: %i\n", start);
	for(int i = 0; i < amount; i++) {
		x[start + i] = (int)std::sin(2 * M_PI * start / amount); 
	}
}

int main() {

	// Shared
	int number_of_points = 1912095500;
	int *array = new int[number_of_points];

	// Private
	int my_thread_id;
	int number_of_threads;
	int points_per_thread;
	int start_location;

	#pragma omp parallel default(shared) private(my_thread_id, number_of_threads, points_per_thread, start_location)
	{
		my_thread_id = omp_get_thread_num();
		number_of_threads = omp_get_num_threads();
		points_per_thread = number_of_points / number_of_threads;
		start_location = my_thread_id * points_per_thread;
		if(my_thread_id == (number_of_threads - 1)) {
			points_per_thread = number_of_points - start_location;
		}
		printf("PPT: %i\n", points_per_thread);
		calculate(array, start_location, points_per_thread);
	}

}
