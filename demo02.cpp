#include <iostream>

int main() {

	#pragma omp parallel
	{
		for(int n=0; n<10; ++n) {
			std::cout << n;
		}
	}
	std::cout << std::endl;
}
