#include <libiomp/omp.h>
#include <iostream>

int main() {

	int flag = 0;

	#pragma omp parallel num_threads(3)
	{
		if(omp_get_thread_num() == 0) {
			#pragma omp atomic update
			flag++;
		} else if(omp_get_thread_num() == 1) {
			#pragma omp flush(flag)
			while (flag < 1) {
				#pragma omp flush(flag)
			}
			printf("Thread 1 awoken\n");

			#pragma omp atomic update
			flag++;
		} else if(omp_get_thread_num() == 2) {
			#pragma omp flush(flag)
			while(flag < 2) {
				#pragma omp flush(flag)
			}
			printf("Thread 2 awoken\n");
		}
	}
}
